from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import *

# Create your views here.
@login_required
def home(request):
    return render(request, 'home.html')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # me refresh tabel user untuk mendapatkan id yang telah di insert
            user.profile.alamat = request.POST['alamat'] #ambil data dari post di html
            form.save()
            raw_password = form.cleaned_data.get('password1') #ambil data dari forms.py
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        return render(request, 'signup.html')
